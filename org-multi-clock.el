;;; org-multi-clock.el --- A package that extends the normal org clock to multiple parallel clocks  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  

;; Author:  Marco Pawłowski <pawlowski.marco@gmail.com>
;; Package-Version: 0.1
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a package for everyone that wants to use multiple org-clocks
;; simultaneously. org-multi-clock tries to be as minimal as possible to be
;; as powerfull as it needs to be. With org-multi-clock you can use
;; org-clock-in and org-clock-out as normally.
;;
;; Additionally it is possible to create multiple new clocks, and switch between them.
;;
;; Usage:
;;
;; The normal working of org-clock-in and org-clock-out is not changed.
;; After clocking in with org-clock-in the function omc-make-new-parallel-clock
;; allowes to clock in an other heading. This creates a new clock and setting
;; it as active clock.
;;
;; Active clock is the currently "visible" clock for org. It is shown in the
;; modline and will be clock out with org-clock-out. To change the currently
;; active clock use omc-set-active-clock.

;;; Code:

(require 'org)
(require 'org-clock)


(defvar org-multi-clock-list ()
  "The list to store the clock data inside")


(defvar org-multi-clock-next-clock-on-remove t
      "Switch to enable/disable automatically setting the next clock as active, when a clock is removed")

;;;###autoload
(defun omc-make-new-parallel-clock ()
  "Create a new clock in parallel"
  (interactive)
  (setq org-clock-marker (make-marker))
  (setq org-clock-hd-marker (make-marker))
  (setq temp-org-clock-resolving-clocks org-clock-resolving-clocks)
  (setq org-clock-resolving-clocks t)
  (remove-hook 'org-clock-in-hook 'omc--add-clock-to-list)
  (org-clock-in)
  (add-hook 'org-clock-in-hook 'omc--add-clock-to-list)
  (setq org-clock-resolving-clocks temp-org-clock-resolving-clocks)
  (omc--add-clock-to-list))


(defun omc--add-clock-to-list ()
  "Adds clock to org-multi-clock-list"
  (setq org-multi-clock-list (cons  `(,org-clock-heading
                               ,org-clock-marker
                               ,org-clock-hd-marker
                               ,org-clock-current-task
                               ,org-clock-effort)
                             org-multi-clock-list)))

(defun omc--set-org-clock-with-running (org-clock-data)
  "Sets the active clock to the given clock"
  (setq org-clock-heading (nth 0 org-clock-data))
  (setq org-clock-marker (nth 1 org-clock-data))
  (setq org-clock-hd-marker (nth 2 org-clock-data))
  (setq org-clock-current-task (nth 3 org-clock-data))
  (setq org-clock-effort (nth 4 org-clock-data))
  
  (save-excursion ; Do not replace this with `with-current-buffer'.
    (with-no-warnings (set-buffer (org-clocking-buffer)))
    (save-restriction
      (widen)
      (goto-char org-clock-marker)
      (beginning-of-line 1)
      (if (and (looking-at (concat "[ \t]*" org-keyword-time-regexp))
	       (equal (match-string 1) org-clock-string))
	  (setq ts (match-string 2)))))
  
  (setq org-clock-start-time (org-time-string-to-time ts))
  ;; add to mode line
  (when (or (eq org-clock-clocked-in-display 'mode-line)
		   (eq org-clock-clocked-in-display 'both))
	   (or global-mode-string (setq global-mode-string '("")))
	   (or (memq 'org-mode-line-string global-mode-string)
	       (setq global-mode-string
		     (append global-mode-string '(org-mode-line-string)))))
  (org-clock-update-mode-line))


(defun omc--get-index-of-clock-hd (hd)
  "Convenient method to get clock with given headline"
  (omc--get-index-of-clock-hd-rec org-multi-clock-list hd 0))


(defun omc--get-index-of-clock-hd-rec (lst hd idx)
  "Recursive search for headline in list of clocks"
  (if (string= hd (nth 0 (nth idx lst)))
      idx
    (omc--get-index-of-clock-hd-rec lst hd (+ idx 1))))

(defun omc--get-clock-list-headings ()
  "Get a list of the headings of all clocks"
  (mapcar 'car org-multi-clock-list))

;;;###autoload
(defun omc-set-active-clock (&optional hd)
  "Selects an active clock from all currently available clocks"
  (interactive (list (completing-read "Select Clock: " (omc--get-clock-list-headings))))
  (when (< 0 (omc--get-amount-of-clocks))
    (omc--set-next-active-clock (omc--get-index-of-clock-hd hd))))


;; https://emacs.stackexchange.com/questions/29786/how-to-remove-delete-nth-element-of-a-list
(defun omc--remove-nth-element (idx list)
  "Convenient method for removing nth element from list"
  (if (zerop idx) (cdr list)
    (let ((last (nthcdr (1- idx) list)))
      (setcdr last (cddr last))
      list)))

(defun omc--remove-org-clock ()
  "Removes the currently active org clock"
  (setq org-multi-clock-list (omc--remove-nth-element
                       (omc--get-index-of-clock-hd org-clock-heading)
                       org-multi-clock-list))
  (when org-multi-clock-next-clock-on-remove
    (omc--set-next-active-clock)))


(defun omc--get-amount-of-clocks ()
  "Gets the number of clocks in the list"
  (length org-multi-clock-list))

(defun omc--set-next-active-clock (&optional idx)
  "Removes clocks when not more available"
  (or idx (setq idx 0))
  (if (< 0 (omc--get-amount-of-clocks))
      (progn
        (if (marker-buffer (nth 1 (nth idx org-multi-clock-list)))
            (omc--set-org-clock-with-running (nth idx org-multi-clock-list))
          (progn
            (message (format "Buffer with clock %s closed." (nth 0 (nth idx org-multi-clock-list))))
            (setq org-multi-clock-list (cdr org-multi-clock-list))
            (omc--set-next-active-clock))))
    (message "No available clocks")))

(defun omc--clock-out-on-closed-buffer ()
  "Check if the current buffer contains a running clock.
If yes, offer to stop it and to save the buffer with the changes."
  (interactive)
  (let ((found nil))
    (dolist (clock org-multi-clock-list)
      (when (and (equal (marker-buffer (nth 1 clock)) (current-buffer))
	         (y-or-n-p (format "Clock-out '%s' in buffer %s before killing it? "
			           (nth 0 clock) (buffer-name))))
        
        (omc--set-org-clock-with-running clock)
        (org-clock-out)
        (setq found t)))
    (when (and found (y-or-n-p "Save changed buffer?")
               (save-buffer)))))

;; Check for running clock before killing a buffer
(remove-hook 'kill-buffer-hook 'org-check-running-clock 'local)
(add-hook 'kill-buffer-hook 'omc--clock-out-on-closed-buffer nil 'local)


(add-hook 'org-clock-in-hook 'omc--add-clock-to-list)
(add-hook 'org-clock-out-hook 'omc--remove-org-clock)


(provide 'org-multi-clock)
;;; org-multi-clock.el ends here
